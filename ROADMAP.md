<!--
SPDX-License-Identifier: AGPL-3.0-or-later
SPDX-FileCopyrightText: Copyright 2022 David Seaward and contributors
-->

# Samosa roadmap (don't hold your breath)

## samosa squash <hash>

```
# git rebase: squash all from <hash> to HEAD
```

## Paging

Identify pager (git config > PAGER > "less" > None)

For all output:
 * "git --no-pager"
 * Then pass to pager ourselves

## Checks

Always check that git exists?
